import {Router} from "express";

import {CourseController} from "../controller/CourseController";

const router = Router()

// restful api
// route parameter
router.get('/', CourseController.all)
router.get('/:courseId', CourseController.one)
router.post('/', CourseController.create)
router.put('/:courseId', CourseController.update)
router.delete('/:courseId', CourseController.delete)

export default router