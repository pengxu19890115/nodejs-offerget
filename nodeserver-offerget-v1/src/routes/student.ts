import {Router} from "express";

import {StudentController} from "../controller/StudentController";

const router = Router()

// restful api
// route parameter
router.get('/', StudentController.all)
router.get('/:studentId', StudentController.one)
router.post('/', StudentController.create)
router.put('/:studentId', StudentController.update)
router.delete('/:studentId', StudentController.delete)

export default router