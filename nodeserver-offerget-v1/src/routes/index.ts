import {Router} from "express";

import student from "./student";
import teacher from "./teacher";
import course from "./course";

const routes = Router()

routes.use('/student', student)
routes.use('/teacher', teacher)
routes.use('/course', course)

export default routes