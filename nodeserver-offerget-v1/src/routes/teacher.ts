import {Router} from "express";

import {TeacherController} from "../controller/TeacherController";

const router = Router()

// restful api
// route parameter
router.get('/', TeacherController.all)
router.get('/:teacherId', TeacherController.one)
router.post('/', TeacherController.create)
router.put('/:teacherId', TeacherController.update)
router.delete('/:teacherId', TeacherController.delete)

export default router