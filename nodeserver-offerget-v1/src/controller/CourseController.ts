import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Course} from "../entity/Course";
import {Err, ErrStr, HttpCode} from "../helper/Err";
import {validate} from "class-validator";


export class CourseController {

    public static get repo() {
        return getRepository(Course)
    }

    static async all(request: Request, response: Response, next: NextFunction) {

    }

    static async one(request: Request, response: Response, next: NextFunction) {

    }

    static async create(request: Request, response: Response, next: NextFunction) {

    }

    static async update(request: Request, response: Response, next: NextFunction) {

    }

    static async delete(request: Request, response: Response, next: NextFunction) {

    }
}