import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Student} from "../entity/Student";
import {Err, ErrStr, HttpCode} from "../helper/Err";
import {validate} from "class-validator";


export class StudentController {

    public static get repo() {
        return getRepository(Student)
    }

    static async all(request: Request, response: Response, next: NextFunction) {

    }

    static async one(request: Request, response: Response, next: NextFunction) {

    }

    static async create(request: Request, response: Response, next: NextFunction) {

    }

    static async update(request: Request, response: Response, next: NextFunction) {

    }

    static async delete(request: Request, response: Response, next: NextFunction) {

    }
}