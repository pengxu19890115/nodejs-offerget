import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Teacher} from "../entity/Teacher";
import {Err, ErrStr, HttpCode} from "../helper/Err";
import {validate} from "class-validator";


export class TeacherController {

    public static get repo() {
        return getRepository(Teacher)
    }

    static async all(request: Request, response: Response, next: NextFunction) {

    }

    static async one(request: Request, response: Response, next: NextFunction) {

    }

    static async create(request: Request, response: Response, next: NextFunction) {

    }

    static async update(request: Request, response: Response, next: NextFunction) {

    }

    static async delete(request: Request, response: Response, next: NextFunction) {

    }
}