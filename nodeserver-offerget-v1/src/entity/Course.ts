import {
    Column,
    CreateDateColumn,
    Entity, JoinTable,
    ManyToMany,
    ManyToOne,
    PrimaryGeneratedColumn,
    Unique,
    UpdateDateColumn
} from "typeorm";
import {IsEmail, Length, Max, Min} from "class-validator";
import {Teacher} from "./Teacher";
import teacher from "../routes/teacher";
import {Student} from "./Student";


@Entity()
export class Course {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @Length(1, 100)
    courseName: string

    @Column()
    @Length(1, 100)
    prerequisites: string;

    @Column({nullable: true, default: false})
    isOnline: boolean;

    @Column()
    classDate: string;

    @Column()
    classTime: number;

    @Column()
    roomNumber: string

    @Column({nullable: true, default: false})
    isActive: boolean;

    @Column({nullable: true, default: false})
    isDelete: boolean;

    @Column()
    @CreateDateColumn()
    create: Date;

    @Column()
    @UpdateDateColumn()
    update: Date;

    //relation
    @ManyToOne(() => Teacher, teacher => teacher.courses)
    teacher: Teacher

    @ManyToMany(() => Student)
    @JoinTable()
    students: Student[]

}