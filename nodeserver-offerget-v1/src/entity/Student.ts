import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, Unique, UpdateDateColumn} from "typeorm";
import {IsEmail, Length, Max, Min} from "class-validator";


@Entity()
@Unique(['email'])
export class Student {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @Length(1, 100)
    firstName: string

    @Column()
    @Length(1, 100)
    lastName: string;

    @Column()
    @IsEmail()
    @Length(5, 150)
    email: string;

    @Column()
    @Min(1)
    @Max(12)
    grade: number;

    @Column()
    @Min(6)
    @Max(22)
    age: number;

    @Column()
    @Length(1, 50)
    gender: string;

    @Column()
    address: string;

    @Column()
    enterYear: string

    @Column({nullable: true, default: false})
    isActive: boolean;

    @Column({nullable: true, default: false})
    isDelete: boolean;

    @Column()
    @CreateDateColumn()
    create: Date;

    @Column()
    @UpdateDateColumn()
    update: Date;

}

