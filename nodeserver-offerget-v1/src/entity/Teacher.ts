import {Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, Unique, UpdateDateColumn} from "typeorm";
import {IsEmail, Length, Max, Min} from "class-validator";
import {Course} from "./Course";


@Entity()
@Unique(['email'])
export class Teacher {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @Length(1, 100)
    firstName: string

    @Column()
    @Length(1, 100)
    lastName: string;

    @Column()
    @IsEmail()
    @Length(5, 150)
    email: string;

    @Column()
    @Length(5, 150)
    department: string;

    @Column()
    @Min(6)
    @Max(22)
    age: number;

    @Column()
    @Length(1, 50)
    gender: string;

    @Column()
    address: string;

    @Column({nullable: true, default: false})
    isActive: boolean;

    @Column({nullable: true, default: false})
    isDelete: boolean;

    @Column()
    @CreateDateColumn()
    create: Date;

    @Column()
    @UpdateDateColumn()
    update: Date;

    // relation
    @OneToMany(() => Course, course => course.teacher)
    courses: Course[]
}